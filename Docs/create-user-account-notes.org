* Create User Account bash script notes

** In the script section
*** First step
#+BEGIN_SRC shell
read -p "Create the user" createusername
#+END_SRC

*** Second step
#+BEGIN_SRC shell
useradd $createusername
#+END_SRC


*** Third step
#+BEGIN_SRC shell
passwd $createusername
#+END_SRC

*** Fourth step
#+BEGIN_SRC shell
usermod -aG wheel "$createusername"
#+END_SRC


*** Fifth step
#+BEGIN_SRC shell
echo "$createusername ALL=(ALL) NOPASSWD: ALL" >> "/etc/sudoers.d/$createusername"
#+END_SRC

*** Sixth
#+BEGIN_SRC shell
echo "This is working now $createusername"
#+END_SRC


** In general version
In this section, this talks about creating the user accounts manually

*** First Step
#+BEGIN_SRC shell
useradd hellouser
#+END_SRC

*** Second step
#+BEGIN_SRC shell
passwd hellouser
#+END_SRC


*** Third Step
#+BEGIN_SRC shell
usermod -aG wheel hellouser
#+END_SRC

*** Fourth Step
#+BEGIN_SRC shell
touch /etc/sudoers.d/hellouser
#+END_SRC

*** Fifth Step
If you want the user to have admin privileges, then use this command to
do so. This can be useful if a teacher, paraprofessional or an educator
is using the device.

#+BEGIN_SRC shell
echo "hellouser ALL=(ALL) NOPASSWD: ALL" >> "/etc/sudoers.d/hellouser"
#+END_SRC

*** Sixth Step
If you want to, it is optionally recommended to make the system rebooted
for further refreshed changes to the device.

#+BEGIN_SRC shell
sudo reboot
#+END_SRC
