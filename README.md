# Smith-Pad-OS 

## Introduction 

Smith-Pad-OS is a Linux based Operating System which is a core of the 
Smith-Pad ecosystem. 


## Features

- CLI Installer for installing Smith-Pad-OS


## Getting Started with developing and maintaining Smith-Pad-OS

In this section, we are going to be talking about getting started 
with developing and maintaining Smith-Pad-OS. 


## What are the branches do? 

Branches in the Smith-Pad-OS contains the following: 

- Desktop Environment Variants 
	- Plasma	= `de-plasma`
	- Cinnamon	= `de-cinnamon`
	- LXDE		= `de-lxde`
	- XFCE		= `de-xfce`

- Other Variants


### Dependencies and Requirements:

In this subsection, we are going to be talking about what are the 
requirements and dependencies we need to install to develop and 
build Smith-Pad-OS. 

- `sudo pacman -S archiso`
- `sudo pacman -S make`
- `sudo pacman -S meson`


### Building Smith-Pad-OS via Docker  

> Currently Working on this. Documentation will be
> Available later.

### How to install Smith-Pad-OS on the device

In this section, we are going to be talking about how to install Smith-Pad-OS
on the device 


#### Manually configuring the Disks
In the Live Image, Manually configuring the disks is required. It is recommended
to use the `cfdisk` program. For the type of partition, choose DOS. Then in the 
first partition, choose the Linux FileSystem, and in the second partition, 
choose the Linux Swap.


Once that is completed, by default, there is a predefined alias, called `runinstaller`
and then it will give you a prompt like this. 

```shell
Welcome to the Installer
--------------------------

Choose the drives to install to...

1. sda
2. vda
3. nvme
```  



