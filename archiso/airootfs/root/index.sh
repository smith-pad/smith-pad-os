## Smith-Pad Installer 

echo '''
Welcome to the Installer
--------------------------

Choose the drives to install to...

1. sda
2. vda
3. nvme
'''

echo -n "" 
read choice


case $choice in
    1)
        echo "sda is selected"
	sh sda.sh
        ;;
    2)
        echo "vda is selected"
	sh vda.sh
	;;
    3)
        echo "nvme is selected"
        sh nvme.sh	
	;;
    *)
        echo "Invalid choice"
        ;;
esac
