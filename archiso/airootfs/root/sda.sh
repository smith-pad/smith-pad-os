## real drive


function formatDisk() 
{
mkfs.ext4 /dev/sda1
mkswap /dev/sda2
}



function mountDisk() 
{
mkdir -pv /mnt/smithpad
mount /dev/sda1 /mnt/smithpad
mkswap /dev/sda2 
}


function initializeBasePackages() 
{
pacman -Syy
genfstab -U /mnt >> /mnt/smithpad/etc/fstab
pacstrap /mnt/smithpad base linux-lts linux-firmware
genfstab -U /mnt/smithpad >> /mnt/smithpad/etc/fstab
}



function chrooting() 
{
arch-chroot /mnt/smithpad ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
arch-chroot /mnt/smithpad pacman -Sy
arch-chroot /mnt/smithpad pacman -Syyu --noconfirm
arch-chroot /mnt/smithpad pacman -S firefox --noconfirm
arch-chroot /mnt/smithpad pacman -S base-devel --noconfirm
arch-chroot /mnt/smithpad pacman -S xorg --noconfirm
arch-chroot /mnt/smithpad pacman -S xorg-xinit --noconfirm
arch-chroot /mnt/smithpad pacman -S base-devel --noconfirm
arch-chroot /mnt/smithpad pacman -S lxde --noconfirm
arch-chroot /mnt/smithpad pacman -S pulseaudio --noconfirm
arch-chroot /mnt/smithpad pacman -S bluetooth --noconfirm
arch-chroot /mnt/smithpad pacman -S networkmanager --noconfirm
arch-chroot /mnt/smithpad pacman -S chromium --noconfirm
arch-chroot /mnt/smithpad rm -rf /etc/X11/xinit/xinitrc
		
}


formatDisk
mountDisk
initializeBasePackages
chrooting

## This will replace to this file
# echo "firefox --kiosk http://localhost:3000" >> /mnt/smithpad/etc/X11/xinit/xinitrc

## Checks if it is working
# cat /mnt/smithpad/etc/X11/xinit/xinitrc





arch-chroot /mnt/smithpad rm -rf /etc/hostname
arch-chroot /mnt/smithpad rm -rf /etc/hosts

echo "device" >> /mnt/smithpad/etc/hostname
cat /mnt/smithpad/etc/hostname


echo """
127.0.0.1	localhost
::1		localhost
127.0.1.1	device 
""" >> /mnt/smithpad/etc/hosts
cat /mnt/smithpad/etc/hosts



arch-chroot /mnt/smithpad pacman -S dhcpcd --noconfirm
arch-chroot /mnt/smithpad systemctl enable dhcpcd
arch-chroot /mnt/smithpad pacman -S grub efibootmgr --noconfirm
arch-chroot /mnt/smithpad grub-install /dev/sda
arch-chroot /mnt/smithpad grub-mkconfig -o /boot/grub/grub.cfg
arch-chroot /mnt/smithpad passwd
clear 
sleep 9

echo """
----------------------------------------------------
-			Rebooting				
-
----------------------------------------------------

"""

reboot
