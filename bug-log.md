## Bugs Issues need to be fixed: 

### 5/18/2024
---




### 5/17/2024
Same issue when booting from GRUB on the ISO image after it
has successfully compiled....

Some issues: 

`::/EFI/BOOT/BOOTx64.EFI: No such file or directory` thing file
came up again. 

**SOLVED** There is a solution to this workaround. 

1. `sudo rm -rf archiso`
2. `git clone https://github.com/archlinux/archiso/`
3. `cd configs`
4. `mv releng ../`
5. `sudo sh build.sh`




### 4/15/2024
Issues when booting from grub. Refuses to boot. Probably have to 
rewrite the entire script as the last resort. This is caused
after the removal of deletion of bootefi.img file in the build.sh
script. Also maybe due to the .img file itself probably too...

**SOLVED**
There is a solution to this workaround. Here are the lists

1. Copy the Files to the a nested folder called `temporary-iso-img` 
2. Then the compile starts.
3. Then move the iso image from the `temporary-iso-img/out` folder to the `Smith-Pad-OS/out` folder
4. Delete the `temporary-iso-img` folder.

This workaround may be complicated but it is better than nothing...


### 4/14/2024
``` shell
[mkarchiso] INFO: Done! Packages installed successfully.
[mkarchiso] INFO: Creating version files...
[mkarchiso] INFO: Done!
[mkarchiso] INFO: Copying /etc/skel/* to user homes...
[mkarchiso] INFO: Done!
[mkarchiso] INFO: Creating a list of installed packages on live-enviroment...
[mkarchiso] INFO: Done!
[mkarchiso] INFO: Setting up SYSLINUX for BIOS booting from a disk...
[mkarchiso] INFO: Preparing kernel and initramfs for the ISO 9660 file system...
[mkarchiso] INFO: Done!
[mkarchiso] INFO: Done! SYSLINUX set up for BIOS booting from a disk successfully.
[mkarchiso] INFO: Setting up SYSLINUX for BIOS booting from an optical disc...
[mkarchiso] INFO: Done! SYSLINUX set up for BIOS booting from an optical disc successfully.
[mkarchiso] INFO: Setting up systemd-boot for x64 UEFI booting...
Can't open /home/user/smith-pad-os/archiso/efiboot.img: No such file or directory
Cannot initialize '::'
::/EFI/BOOT/BOOTx64.EFI: No such file or directory
```

> When cloning the repository fresh, then compiling the iso
> the first time, there are no errors. However, when running 
> the `sudo sh build.sh` again, the BOOTx64 error shows up 
> then

**SOLVED**
Here are the solutions.

- Disabled removal of `efiboot.img` in build.sh

``` shell

```


### 12/1/2023

``` shell
archiso/base._make_boot_on_fat
archiso/base._make_common_bootmode_systemd-boot
archiso/base._make_common_bootmode_systemd-boot_conf.esp
archiso/base._make_common_bootmode_systemd-boot_conf.isofs
archiso/base._make_common_grubenv_and_loopbackcfg
```

>Everytime, this is added to the .gitignore there are problems 
>when finding the efi boot file to it. 

**SOLVED**
- To solve this problem, you need to use this command. 

```shell
git stash -u
```

**SOLVED**
- This already been solved by adding these directories in a gitignore file

```shell
------------------
File: .gitignore
------------------
base._cleanup_pacstrap_dir                          
base._make_bootmode_bios.syslinux.eltorito          
build._build_buildmode_iso
base._make_bootmode_bios.syslinux.mbr               
build_date
base._make_bootmode_uefi-x64.systemd-boot.eltorito  
base._make_bootmode_uefi-x64.systemd-boot.esp       
efiboot.img
base._make_boot_on_iso9660                          
iso/
base._make_custom_airootfs                          
iso._build_iso_image
base._make_customize_airootfs                       
iso.pacman.conf
base._make_packages                                 
base._make_pacman_conf                              
base._make_pkglist                                  
base._make_version                                  
base._mkairootfs_squashfs
x86_64
out

archiso/base._make_boot_on_fat
archiso/base._make_common_bootmode_systemd-boot
archiso/base._make_common_bootmode_systemd-boot_conf.esp
archiso/base._make_common_bootmode_systemd-boot_conf.isofs
archiso/base._make_common_grubenv_and_loopbackcfg

```
**SOLVED**
