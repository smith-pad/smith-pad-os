## setup.sh 
## This script allows the ability to setup the dependencies for building it




## This is where it updates the packages
sudo pacman -Syyu --noconfirm


## This is where it installs the archiso
sudo pacman -S archiso --noconfirm

## This is where it installs the qemu-full package
sudo pacman -S qemu-full --noconfirm


## This is where it installs the ranger package
sudo pacman -S ranger --noconfirm


## This is where it installs the neovim package
sudo pacman -S neovim --noconfirm

## This is where it installs the meson package
sudo pacman -S meson --noconfirm

## This is where it installs the cmake package
sudo pacman -S cmake --noconfirm
